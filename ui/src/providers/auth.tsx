import React, { ReactElement } from "react";
import { Auth0Provider } from "@auth0/auth0-react";

interface Props {
  children: ReactElement;
}

export const AuthProvider: React.FC<Props> = ({ children }) => {
  const domain = process.env.REACT_APP_AUTH0_DOMAIN || "";
  const clientId = process.env.REACT_APP_AUTH0_CLIENT_ID || "";
  const audience = `https://${process.env.REACT_APP_AUTH0_DOMAIN}/api/v2/`;

  return (
    <Auth0Provider
      domain={domain}
      clientId={clientId}
      redirectUri={window.location.origin}
      audience={audience}
      cacheLocation="localstorage"
    >
      {children}
    </Auth0Provider>
  );
};
