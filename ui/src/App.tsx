import React, { useEffect } from "react";
import logo from "./logo.svg";
import "./App.css";
import { AuthProvider } from "./providers/auth";
import { useAuth0 } from "@auth0/auth0-react";
import { security } from "./helpers/security";

const LoginButton = () => {
  const { user, loginWithRedirect } = useAuth0();
  const { getAccessTokenSilently, getAccessTokenWithPopup } = useAuth0();
  // security.setAccessTokenSilently(getAccessTokenSilently);

  security.setAccessTokenSilently(getAccessTokenSilently);
  // useEffect(() => {
  //   func(getAccessTokenSilently);
  // }, [func, getAccessTokenSilently]);

  func();

  return (
    <>
      <p>{user?.email}</p>
      <button onClick={() => loginWithRedirect()}>Log In</button>{" "}
      <button onClick={() => getAccessTokenWithPopup()}>Get token</button>{" "}
    </>
  );
};

const func = async () => {
  const token = await security.getAccessTokenSilently()();

  console.log(token);
};

function App() {
  return (
    <div className="App">
      <AuthProvider>
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <p>This is {process.env.REACT_APP_ENVIRONMENT}</p>
          <LoginButton />
        </header>
      </AuthProvider>
    </div>
  );
}

export default App;
