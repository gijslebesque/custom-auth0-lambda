module "prod" {
    source = "../modules"
    auth0_domain = var.staging_auth0_domain
    auth0_client_id = var.staging_auth0_client_id
    auth0_client_secret = var.staging_auth0_client_secret
    project_name = var.staging_project_name
}