module "prod" {
    source = "../modules"
    auth0_domain = var.prod_auth0_domain
    auth0_client_id = var.prod_auth0_client_id
    auth0_client_secret = var.prod_auth0_client_secret
    project_name = var.prod_project_name
}