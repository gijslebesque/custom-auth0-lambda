import jwksClient from "jwks-rsa";
import jwt from "jsonwebtoken";
import {
  APIGatewayEvent,
  APIGatewayProxyEventHeaders,
  PolicyDocument,
} from "aws-lambda";

const { AUTH0_DOMAIN } = process.env;
const AUTH0_URL = `https://${AUTH0_DOMAIN}/`;

const getToken = (headers: APIGatewayProxyEventHeaders) => {
  const authorizationBearer = headers?.authorization;
  if (!authorizationBearer) {
    throw new Error('Expected "headers.authorization" parameter to have value');
  }

  const match = authorizationBearer.match(/^Bearer (.*)$/);
  if (!match || match.length < 2) {
    throw new Error(
      `Invalid Authorization token - ${authorizationBearer} does not match "Bearer .*"`
    );
  }
  return match[1];
};

const jwtOptions = {
  audience: `${AUTH0_URL}api/v2/`,
  issuer: AUTH0_URL,
};

const client = jwksClient({
  cache: true,
  rateLimit: true,
  jwksRequestsPerMinute: 10, // Default value
  jwksUri: `${AUTH0_URL}.well-known/jwks.json`,
});

export const getPolicyDocument = (
  effect: "Allow",
  resource: string
): PolicyDocument => {
  return {
    Version: "2012-10-17", // default version
    Statement: [
      {
        Action: "execute-api:Invoke", // default action
        Effect: effect,
        Resource: resource,
      },
    ],
  };
};

interface IKey {
  publicKey?: string;
  rsaPublicKey?: string;
}

export const authoriser = async (event: APIGatewayEvent) => {
  const token = getToken(event.headers); //jwt

  console.log("audience", `${AUTH0_URL}/api/v2`);

  const decoded = jwt.decode(token, { complete: true });

  if (!decoded || !decoded.header || !decoded.header.kid) {
    throw new Error("invalid token");
  }

  const key: IKey = await client.getSigningKey(decoded.header.kid);

  const signingKey = key?.publicKey || key?.rsaPublicKey;

  if (!signingKey) {
    throw new Error("invalid token");
  }

  return jwt.verify(token, signingKey, jwtOptions);
};
